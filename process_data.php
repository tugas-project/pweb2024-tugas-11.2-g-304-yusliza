<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Ambil data dari form
    $nama = $_POST["nama"];
    $nim = $_POST["nim"];
    $kelas = $_POST["kelas"];
    $semester = $_POST["semester"];
    $fakultas = $_POST["fakultas"];
    $prodi = $_POST["prodi"];
    $mata_kuliah = $_POST["mata_kuliah"];
    $nilai = $_POST["nilai"];
    $dosen_pengajar = $_POST["dosen_pengajar"];

    // Hitung jumlah mata kuliah yang diinputkan
    $jumlah_mata_kuliah = count($mata_kuliah);

    // Tampilkan hasil dalam tabel
    echo "<!DOCTYPE html>";
    echo "<html lang='en'>";
    echo "<head>";
    echo "<meta charset='UTF-8'>";
    echo "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
    echo "<title>Hasil Ujian Akhir Semester Genap</title>";
    echo "<link rel='stylesheet' href='style.css'>";
    echo "</head>";
    echo "<body>";
    echo "<header>";
    echo "<h1>Hasil Ujian Akhir Semester Genap</h1>";
    echo "</header>";
    echo "<div class='result'>";
    echo "<table>";
    echo "<tr><th>Nama</th><td>$nama</td></tr>";
    echo "<tr><th>NIM</th><td>$nim</td></tr>";
    echo "<tr><th>Kelas</th><td>$kelas</td></tr>";
    echo "<tr><th>Semester</th><td>$semester</td></tr>";
    echo "<tr><th>Fakultas</th><td>$fakultas</td></tr>";
    echo "<tr><th>Program Studi</th><td>$prodi</td></tr>";
    echo "<tr><th colspan='4'>Daftar Mata Kuliah</th></tr>";
    echo "<tr><th>Mata Kuliah</th><th>Nilai</th><th>Keterangan</th><th>Dosen Pengajar</th></tr>";
    
    // Iterasi untuk setiap mata kuliah
    for ($i = 0; $i < $jumlah_mata_kuliah; $i++) {
        // Tentukan keterangan lulus atau tidak lulus berdasarkan nilai
        $keterangan = ($nilai[$i] >= 60) ? "LULUS" : "TIDAK LULUS";
        
        // Tampilkan baris tabel untuk mata kuliah
        echo "<tr>";
        echo "<td>$mata_kuliah[$i]</td>";
        echo "<td>$nilai[$i]</td>";
        echo "<td>$keterangan</td>"; // Menampilkan keterangan lulus/tidak lulus
        echo "<td>$dosen_pengajar[$i]</td>"; // Menampilkan dosen pengajar
        echo "</tr>";
    }
    echo "</table>";
    echo "</div>";
    echo "</body>";
    echo "</html>";
}
?>
